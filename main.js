function createNewUser(){
    let createNewUser = {
      getLogin: function() {
        let loginName = this.firstName[0] + "." + this.lastName;
        loginName = loginName.toLowerCase();
        return loginName;
      }
    }
    
    createNewUser.firstName = prompt("Enter your first name");
    createNewUser.lastName = prompt("Enter your last name");
    return createNewUser;
  }
  
    let newUser = createNewUser();
  
  console.log(newUser.getLogin());

  /* 
  1. Об'єкт - масив даних. Він потрібен, щоб деяка інформація зберігалася у ньому, а не окремо. Наприклад інфо про користувача. Дуже зручно, коли вона уся буде зберігатися в одному об'єкті (масиві), а не в окремих змінних.
  
  2. Будь-який тип даних, як у звичайній змінній.
  
  3.У змінній зберігається не сам об'єкт, а посилання на нього. Тобто коли ми присвоюємо змінній значення у вигляді об'єкту, то JS створює ділянку у пам'яті під створенний об'єкт, а у саму змінну зберігається посилання на цю ділянку пам'яті. Якщо скопіювати об'єкт через "=", то зберігається не копія об'єкта, а посилання на об'єкт.
  */